const fs = require('fs');
const exec = require('child_process').exec;

const w1Path = '/sys/bus/w1/devices/'; 
const busMaster = 'w1_bus_master';
const slave = '/w1_slave';

function Sensors() {
    this.sensors = [];
    
    files = fs.readdirSync(w1Path);
    files.forEach(function (sensorName){
        if (sensorName.indexOf(busMaster) == 0) {
            return;
        } 

        var sensorPath = w1Path + sensorName + slave;

        var sensor = {
            name: sensorName,
            path: sensorPath,
            value: null
        };

        that._readSensorAtPath(w1_sensor, function(value) {
            sensor.value = value;
            sensor.timestamp = Date.now();
        });

        that.sensors.push(sensor);
    });

}

module.exports = Sensors;

Sensors.prototype.hasSensors = function(){
    return this.sensors.length > 0;
}

Sensors.prototype.getSensors = function(){
    return this.sensors;
}

Sensors.prototype._readSensorAtPath = function(path, callback){
    exec("cat " + path + " | grep t= | cut -f2 -d= | awk '{print $1/1000}'", function( error, stdout, stderr ) {
        if (error) { callback(error); }
        callback( parseFloat(stdout) );
    });
}