const express = require('express');
const Raspistill = require('node-raspistill').Raspistill;
const Sensors = require('bin/sensors');

// API CONF
const app = express();

// RASPISTILL CONF
const camera = new Raspistill({
    noFileSave: true
});

camera.setOptions({
    saturation: -100
});


// ROUTES
app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/picture', function(req,res) {
    camera.takePhoto().then(
        (photo) => {
            res.setHeader('Content-Type','image/jpeg');
            res.status(200).send(photo);
        },
        (error) => {
            console.error(error);
            res.setHeader('Content-Type','text/plain');
            res.status(503).send('503 - IMPOSSIBLE TO TAKE PHOTO');
        }
    );
});

app.get('/temperature', function(req,res) {
    const sens = new Sensors();
    if(sens.hasSensors()){
        res.setHeader('Content-Type','application/json');
        res.status(200).send(sens.getSensors());
    } else {
        res.setHeader('Content-Type','text/plain');
        res.status(204).send('204 - NO SENSOR');
    }
});

app.use(function(req,res,next) {
    res.setHeader('Content-Type','text/plain');
    res.status(404).send('404 - URI NOT FOUND');
});

module.exports = app;