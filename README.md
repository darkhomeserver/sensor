# Sensor

Sensor est une API REST en Node.JS destinée à être déployée sur des raspberry pi. 
Le but est de créer une interface entre les différents capteurs branchés sur les entrées du raspberry (GPIO, camera), et des clients externes.

## TO DO 

  * Terminer la version basique de l'API (camera et capteur de température)
  * Sécuriser l'accès aux ressources
  
## DONE 

  * Lien entre node.js et la caméra du raspberry pi

  